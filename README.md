# KRun

A simple tool to run ad-hoc scripts in kubernetes

While there are very good solutions for FAAS and continuous delivery in kubernetes, they are optimised for write once, execute many operations.

When batch processes need to run single use scripts, building and deploying a container image is complex and can be overkill

Krun uses a different way of running single use scripts, instead of building a container image per invocation, it uses a "base layer" container image for the script runtime, and injects script context at runtime before executing the container.


## Usage

That part is pretty easy, just create a standard kubernetes pod yaml and a folder containing your ad-hoc script and necessary data files.

then run 
```sh
krun pod.yaml contextFolder
```

And enjoy

Krun will create the pod, inject the provided folder in /app, execute the pod, print out the pod logs in real time, and finally destroy the pod after run, leaving the cluster in a clean state


## Cluster dependencies

Well, you need a kubernetes cluster of course, other than that, all that is needed is to deploy a context caching service.

### Easy mode

Deploy the following in the namespace in which you will run krun scripts

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: krun-datacache
  namespace: "[YOUR NAMESPACE HERE]"
spec:
  selector:
    matchLabels:
      run: krun-datacache
  replicas: 1
  template:
    metadata:
      labels:
        run: krun-datacache
    spec:
      containers:
        - name: datacache-server
          image: registry.gitlab.com/pujo.j/dircache:0.1.0
          ports:
            - containerPort: 8080
          args:
            - server
---
apiVersion: v1
kind: Service
metadata:
  name: krun-datacache
  labels:
    run: krun-datacache
spec:
  ports:
    - port: 8080
      protocol: TCP
  selector:
    run: krun-datacache
```
### High availability mode

If you need a high availability context cache service, you need a backing store, that will depend on your cloud provider/object store

TODO


