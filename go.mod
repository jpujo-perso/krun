module krun

go 1.13

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	gitlab.com/pujo.j/dircache v0.0.0-20191016151842-8b6716d9834f
	k8s.io/api v0.0.0-20190620084959-7cf5895f2711
	k8s.io/apimachinery v0.0.0-20190612205821-1799e75a0719
	k8s.io/client-go v0.0.0-20190620085101-78d2af792bab
)
