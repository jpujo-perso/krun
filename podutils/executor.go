/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package podutils

import (
	"bytes"
	"context"
	"encoding/hex"
	"errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/pujo.j/dircache/simplevfs"
	"io"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strconv"

	"crypto/rand"
	//"k8s.io/apimachinery/pkg/util/net"
	"k8s.io/apimachinery/pkg/util/yaml"
	k8s "k8s.io/client-go/kubernetes"
	"strings"
	"sync"
)

type DataCacheConfig struct {
	Name string
	Port int
}

type Job struct {
	PodDescriptor   []byte
	AppFolder       string
	TimeoutS        uint64
	DataCacheConfig DataCacheConfig
	RemoteDir       string
	events          chan *corev1.Pod
	client          *k8s.Clientset
	pod             *corev1.Pod
	podLock         sync.RWMutex
	key             string
}

func (j *Job) Pod() *corev1.Pod {
	j.podLock.RLock()
	defer j.podLock.RUnlock()
	return j.pod
}

var initDesc = `
name: krun-init
image: registry.gitlab.com/pujo.j/dircache:0.1.6
args:
  - "get"
  - "-b"
  - "http://krun-datacache:443"
  - "-d"
  - "/data"
resources:
  requests:
    memory: 250Mi
    cpu: .25
  limits:
    memory: 250Mi
    cpu: .25
volumeMounts:
  - name: data
    mountPath: /data
    readOnly: false
`

func PrepareContext(ctx context.Context, job *Job, clientset *k8s.Clientset) error {
	job.client = clientset
	var pod = corev1.Pod{}
	decoder := yaml.NewYAMLOrJSONDecoder(bytes.NewReader(job.PodDescriptor), 8192)
	err := decoder.Decode(&pod)
	if err != nil {
		log.WithError(err).Error("Parsing pod")
		return err
	}
	job.pod = &pod
	fs := simplevfs.NewVFS()
	err = simplevfs.AddLocalDir(fs, job.AppFolder, fs.Root)
	if err != nil {
		return err
	}
	pipeReader, pipeWriter := io.Pipe()
	doneChan := make(chan error)
	go func() {
		err := simplevfs.TarGz(fs, pipeWriter)
		if err != nil {
			doneChan <- err
		} else {
			close(doneChan)
		}
		_ = pipeWriter.Close()
	}()
	job.client.RESTClient()
	post := job.client.RESTClient().Post().
		Context(ctx).
		RequestURI("/api/v1/namespaces/" + pod.Namespace + "/services/http:" + job.DataCacheConfig.Name + ":" + strconv.Itoa(job.DataCacheConfig.Port) + "/proxy/").
		Body(pipeReader)
	log.WithField("request", post).Debug("Sending context")
	res := post.Do()
	log.WithField("response", res).Debug("Sent context")
	if res.Error() != nil {
		return res.Error()
	}
	var statusCode int
	res.StatusCode(&statusCode)
	if statusCode != 201 {
		return errors.New("http " + strconv.Itoa(statusCode))
	}
	body, err := res.Raw()
	if err != nil {
		return err
	}
	job.key = strings.Trim(string(body), "\n\r ")
	log.WithField("key", job.key).Debug("got context key")
	return nil
}

func StartJob(ctx context.Context, job *Job) error {
	job.events = make(chan *corev1.Pod, 100)
	var pod = corev1.Pod{}
	decoder := yaml.NewYAMLOrJSONDecoder(bytes.NewReader(job.PodDescriptor), 8192)
	err := decoder.Decode(&pod)
	if err != nil {
		log.WithError(err).Error("Parsing pod")
		return err
	}
	// Modify pod name, append random
	rnd_bytes := make([]byte, 16)
	_, err = rand.Read(rnd_bytes)
	if err != nil {
		return err
	}
	rnd_string := hex.EncodeToString(rnd_bytes)
	pod_name := pod.ObjectMeta.Name + "-" + rnd_string
	pod.ObjectMeta.Name = pod_name
	if pod.ObjectMeta.Labels == nil {
		pod.ObjectMeta.Labels = make(map[string]string)
	}
	pod.ObjectMeta.Labels["krun"] = pod_name
	// Edit pod to add data volume
	pod.Spec.Volumes = append(pod.Spec.Volumes, corev1.Volume{
		Name: "data",
		VolumeSource: corev1.VolumeSource{
			EmptyDir: &corev1.EmptyDirVolumeSource{},
		},
	})
	// Inject file copy init-container
	firstInit := corev1.Container{}
	decoder = yaml.NewYAMLOrJSONDecoder(strings.NewReader(initDesc), 8192)
	err = decoder.Decode(&firstInit)
	if err != nil {
		log.WithError(err).Panic("parsing internal yaml for init-container")
	}
	// add context key
	firstInit.Args = append(firstInit.Args, job.key)
	pod.Spec.InitContainers = append([]corev1.Container{firstInit}, pod.Spec.InitContainers...)
	//Edit first container to mount volume /app/script
	pod.Spec.Containers[0].VolumeMounts = append(pod.Spec.Containers[0].VolumeMounts, corev1.VolumeMount{
		Name:      "data",
		ReadOnly:  true,
		MountPath: job.RemoteDir,
	})
	// Start pod
	startedPod, err := job.client.CoreV1().Pods(pod.Namespace).Create(&pod)
	if err != nil {
		log.WithError(err).Error("Starting pod")
		return err
	}
	job.pod = startedPod
	// Listen for pod events
	go func() {
	OuterLoop:
		for {
			var watchTimoutSeconds int64 = 60 * 5
			watch, err := job.client.CoreV1().Pods(job.pod.Namespace).Watch(metav1.ListOptions{
				Watch:          true,
				FieldSelector:  "metadata.name=" + startedPod.Name,
				TimeoutSeconds: &watchTimoutSeconds,
			})
			if err != nil {
				log.WithError(err).Error("Running watch")
			} else {
			InnerLoop:
				for {
					select {
					case e, ok := <-watch.ResultChan():
						if !ok {
							log.Debug("Restarting watch")
							break InnerLoop
						} else {
							pod, ok := e.Object.(*corev1.Pod)
							if ok {
								job.events <- pod
							}
						}
					case <-ctx.Done():
						break OuterLoop
					}

				}
			}

		}
	}()
	return nil
}

func (j *Job) WaitForPodRunning(ctx context.Context) error {
	pod := j.Pod()
	if pod != nil {
		phase := j.Pod().Status.Phase
		if phase == corev1.PodRunning || phase == corev1.PodSucceeded || phase == corev1.PodFailed {
			return nil
		}
	}
	for {
		select {
		case pod := <-j.events:
			{
				log.WithField("pod", pod.Name).WithField("phase", pod.Status.Phase).Debug("pod event received")
				j.podLock.Lock()
				j.pod = pod
				j.podLock.Unlock()
				if pod.Status.Phase == corev1.PodRunning || pod.Status.Phase == corev1.PodSucceeded || pod.Status.Phase == corev1.PodFailed {
					return nil
				}
			}
		case <-ctx.Done():
			{
				return errors.New("timeout waiting for pod creation")
			}
		}
	}
}

func (j *Job) CopyLogs(ctx context.Context, output io.Writer) error {
	err := j.WaitForPodRunning(ctx)
	if err != nil {
		return err
	}
	pod := j.Pod()
	log.WithField("pod", pod.Name).Debug("Requesting pod logs")
	var goBack int64 = 60 * 5
	logRequest := j.client.CoreV1().Pods(pod.Namespace).GetLogs(pod.Name, &corev1.PodLogOptions{
		Follow:       true,
		SinceSeconds: &goBack,
	})
	reader, err := logRequest.Stream()
	if err != nil {
		return err
	}
	res := make(chan error)
	go func() {
		_, err = io.CopyBuffer(output, reader, []byte{0})
		res <- err
	}()
	select {
	case r := <-res:
		return r
	case <-ctx.Done():
		return nil
	}
}

func (j *Job) GetExitCode(ctx context.Context) (code int32, err error) {
	defer func() {
		if r := recover(); r != nil {
			log.WithField("in", r).Error("Recovered from panic GetExitCode")
			code = 3
			err = errors.New("panic in GetExitCode")
		}
	}()
	pod := j.Pod()
	phase := pod.Status.Phase
	if phase == corev1.PodSucceeded || phase == corev1.PodFailed {
		return pod.Status.ContainerStatuses[0].State.Terminated.ExitCode, nil
	}
	for {
		select {
		case <-ctx.Done():
			{
				err := errors.New("timeout waiting for pod termination")
				log.Debug(err.Error())
				return 2, err
			}
		case pod = <-j.events:
			{
				phase = pod.Status.Phase
				if phase == corev1.PodSucceeded || phase == corev1.PodFailed {
					return pod.Status.ContainerStatuses[0].State.Terminated.ExitCode, nil
				}
			}
		}
	}
}

func (j *Job) Close() error {
	pod := j.Pod()
	log.WithField("pod", pod.Name).Info("Cleaning up")
	deletePolicy := metav1.DeletePropagationForeground
	gracePeriod := int64(10)
	err := j.client.CoreV1().Pods(pod.Namespace).Delete(pod.Name, &metav1.DeleteOptions{
		GracePeriodSeconds: &gracePeriod,
		PropagationPolicy:  &deletePolicy,
	})
	if err != nil {
		log.WithError(err).Error("Deleting pod")
	} else {
		log.WithField("pod", pod.Name).Info("Pod deleted")
	}
	return err
}
