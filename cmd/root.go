/*
 * Copyright 2019 Josselin PUJO
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cmd

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"io/ioutil"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
	"k8s.io/client-go/tools/clientcmd"
	"krun/podutils"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"
)

var debug bool
var kubconfig string

var timeout string

var rootCmd = &cobra.Command{
	Use:     "krun [yamlFile] [folder]",
	Short:   "A scripting k8s client",
	Long:    "Run one-of multi-file scripts without creating a custom docker image",
	Version: "0.1.0",
	Args:    cobra.ExactArgs(2),
	PreRun: func(cmd *cobra.Command, args []string) {
		if debug {
			log.SetLevel(log.DebugLevel)
		} else {
			log.SetLevel(log.InfoLevel)
		}
	},
	Run: func(cmd *cobra.Command, args []string) {
		timeout_duration, err := time.ParseDuration(timeout)
		if err != nil {
			log.WithError(err).Errorf("cannot parse duration %s", timeout)
			os.Exit(1)
		}
		err = doJob(args[0], args[1], timeout_duration)
		if err != nil {
			log.WithError(err).Error()
			errorCode, ok := err.(ExitError)
			if ok {
				os.Exit(int(errorCode))
			} else {
				os.Exit(1)
			}
		}
	},
}

func doJob(yamlFileName string, scriptFolder string, timeout time.Duration) error {
	// Force 0 paralellism to reduce cache chatter and high select costs
	runtime.GOMAXPROCS(1)
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		s := <-sigs
		if s != nil {
			log.WithField("Signal", s).Info("Received system signal, cancelling execution")
			cancel()
		}
	}()
	var jobDesc []byte
	var err error
	if yamlFileName == "-" {
		jobDesc, err = ioutil.ReadAll(os.Stdin)
	} else {
		jobDesc, err = ioutil.ReadFile(yamlFileName)
	}
	if err != nil {
		log.WithError(err).Error("Reading yaml file")
		return err
	}
	if kubconfig == "" {
		// Check env variable
		kubConfigEnv, ok := os.LookupEnv("KUBECONFIG")
		if ok {
			kubconfig = kubConfigEnv
		} else {
			if home := homeDir(); home != "" {
				kubconfig = filepath.Join(home, ".kube", "config")
			} else {
				log.Error("No home dir, kubeconfig flag is mandatory")
				return err
			}
		}
	}
	config, err := clientcmd.BuildConfigFromFlags("", kubconfig)
	if err != nil {
		log.WithError(err).Error("Loading k8s configuration")
		return err
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.WithError(err).Error("Creating kubernetes client")
		return err
	}
	log.WithField("client", client).Debug("Created k8s client")
	job := &podutils.Job{
		PodDescriptor: jobDesc,
		AppFolder:     scriptFolder,
		RemoteDir:     remoteDirName,
		TimeoutS:      uint64(timeout.Seconds()),
		DataCacheConfig: podutils.DataCacheConfig{
			Name: dataCacheName,
			Port: dataCachePort,
		},
	}
	err = podutils.PrepareContext(ctx, job, client)
	if err != nil {
		log.WithError(err).Error("Preparing job")
		return err
	}
	err = podutils.StartJob(ctx, job)
	if err != nil {
		log.WithError(err).Error("Starting pod")
		return err
	}
	defer func() {
		err := job.Close()
		if err != nil {
			log.WithError(err).Error("Stopping pod")
		}
	}()
	err = job.WaitForPodRunning(ctx)
	if err != nil {
		log.WithError(err).Error("Waiting for pod to run")
		return err
	}
	log.WithField("pod", job.Pod().Name).Info("Started pod")
	err = job.CopyLogs(ctx, os.Stdout)
	if err != nil {
		log.WithError(err).Error("Copying logs to stdout")
	}
	exitCode, err := job.GetExitCode(ctx)
	if err != nil {
		return err
	} else {
		if exitCode != 0 {
			return ExitError(exitCode)
		}
		return nil
	}
}

type ExitError int

func (e ExitError) Error() string {
	return fmt.Sprintf("Exit Code: %d", e)
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return os.Getenv("USERPROFILE") // windows
}

var dataCacheName string
var dataCachePort int
var remoteDirName string

func init() {
	rootCmd.PersistentFlags().BoolVarP(&debug, "verbose", "v", false, "Verbose Logging")
	rootCmd.PersistentFlags().StringVarP(&kubconfig, "kubeconfig", "k", "", "path to kubeconfig file")
	rootCmd.PersistentFlags().StringVarP(&timeout, "timeout", "t", "15m", "timeout duration")
	rootCmd.PersistentFlags().StringVarP(&dataCacheName, "datacachename", "d", "krun-datacache", "name of the datacache service")
	rootCmd.PersistentFlags().IntVarP(&dataCachePort, "datacacheport", "p", 443, "port of the datacache service")
	rootCmd.PersistentFlags().StringVarP(&remoteDirName, "remotedir", "r", "/app", "Path where files are to be deployed on the target container")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Error(err.Error())
	}
}
